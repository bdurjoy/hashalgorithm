import java.util.ArrayList;
import java.util.Scanner;


public class Lab8 {
	public static void main(String[] args) {
		ArrayList<String> dataItems = new ArrayList<String>(100);
		for(int i = 0; i < 100; i++) {
			dataItems.add(i, null);
		}
		Scanner keyboard = new Scanner(System.in);
		boolean finish = false;
		
		while(finish == false) {
			System.out.println("Enter"
					+ "\t1. Add a String to the array dataItems\n"
					+ "\t2. Search for a String in the array dataitems\n"
					+ "\t3. To Quit");
			String userInput = keyboard.next();
			
			if(userInput.equals("1")) {
				System.out.println("Enter a string to add the list: ");
				String stringInput = keyboard.next();
				//write all the function to write to the string
				int a = (int) stringInput.charAt(0);
				int b = 0;
				if(stringInput.length()>1) {
					b = (int) stringInput.charAt(1);
				}
				int index = (a+b)%100;
				if(dataItems.get(index) == null) {
					dataItems.add(index, stringInput);
					System.out.println("String placed in "+index+" position");
				}else {
					for(int insert = index+1; insert< 100; insert++) {
						if(insert == 99) {
							System.out.println("Sorry! String "+stringInput+" could not place in the list");
							break;
						}
						if(dataItems.get(insert) == null) {
							dataItems.add(insert, stringInput);
							System.out.println("String placed in "+insert+" position");
							break;
						}
					}
				}
			}else if(userInput.equals("2")) {
				//search for string and
				System.out.println("Enter a string to search: ");
				String stringSearch = keyboard.next();
				int a = (int) stringSearch.charAt(0);
				int b = 0;
				if(stringSearch.length()>1) {
					b = (int) stringSearch.charAt(1);
				}
				int index = (a+b)%100;
				if(dataItems.get(index) == null) {
					System.out.println(stringSearch+" is not available in the list.");
				}else {
					for(int search = index; search < 100; search++) {
						if(dataItems.get(search).contains(stringSearch)) {
							System.out.println("String "+stringSearch+" is in the position "+search);
							break;
						}
					}
				}
			}else if(userInput.equals("3")) {
				System.out.println("Exit!!!");
				finish = true;
			}else {
				System.out.println("Sorry! Invalid Input, and Try Again");
			}
		}
	}
}
